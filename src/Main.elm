port module Main exposing (..)

import Audio exposing (Audio, AudioCmd, AudioData)
import Browser
import Browser.Dom
import Browser.Events
import Duration exposing (Duration)
import Html exposing (Html, div, text)
import Html.Attributes as Attr
import Html.Events as Ev
import Json.Decode
import Json.Encode
import Quantity
import Task
import Time


type alias TimerSettings =
    { name : String
    , session : Duration
    , break : Duration
    , longBreak : Duration
    , sessionsBeforeLongBreak : Int
    }


pomodoro : TimerSettings
pomodoro =
    { name = "Pomodoro"
    , session = Duration.minutes 25
    , break = Duration.minutes 5
    , longBreak = Duration.minutes 20
    , sessionsBeforeLongBreak = 4
    }



---- MODEL ----


type Model
    = Loading
    | Errored String
    | Loaded LoadedModel


type alias LoadedModel =
    { playing : Bool
    , noise : Audio.Source
    , startTime : Time.Posix
    }


init : flags -> ( Model, Cmd Msg, AudioCmd Msg )
init _ =
    ( Loading
    , Cmd.none
    , Audio.loadAudio AudioLoaded "audio/white-noise.wav"
    )



---- UPDATE ----


type Msg
    = ClickedToggle
    | PressedSpace
    | GotStartTime Time.Posix
    | AudioLoaded (Result Audio.LoadError Audio.Source)
    | NoOp


update : AudioData -> Msg -> Model -> ( Model, Cmd Msg, AudioCmd Msg )
update audioData msg model =
    case model of
        Loading ->
            case msg of
                AudioLoaded (Ok audioSource) ->
                    ( Loaded
                        { playing = False
                        , noise = audioSource
                        , startTime = Time.millisToPosix 0
                        }
                    , Cmd.none
                    , Audio.cmdNone
                    )

                AudioLoaded (Err loadError) ->
                    let
                        message =
                            case loadError of
                                Audio.FailedToDecode ->
                                    "Failed to decode audio"

                                Audio.NetworkError ->
                                    "Network error"

                                Audio.UnknownError ->
                                    "Unknown error"

                                Audio.ErrorThatHappensWhenYouLoadMoreThan1000SoundsDueToHackyWorkAroundToMakeThisPackageBehaveMoreLikeAnEffectPackage ->
                                    "Error that happens when you load more than 1000 sounds due to hacky work around to make this package behave more like an effect package"
                    in
                    ( Errored message, Cmd.none, Audio.cmdNone )

                _ ->
                    ( model, Cmd.none, Audio.cmdNone )

        Errored _ ->
            ( model, Cmd.none, Audio.cmdNone )

        Loaded loadedModel ->
            updateLoaded msg loadedModel
                |> (\( loadedModel_, cmd, audioCmd ) ->
                        ( Loaded loadedModel_, cmd, audioCmd )
                   )


updateLoaded : Msg -> LoadedModel -> ( LoadedModel, Cmd Msg, AudioCmd Msg )
updateLoaded msg model =
    case msg of
        PressedSpace ->
            ( model, Task.perform GotStartTime Time.now, Audio.cmdNone )

        ClickedToggle ->
            ( model
            , Cmd.batch [ Task.perform GotStartTime Time.now, Browser.Dom.blur "toggle" |> Task.attempt (\_ -> NoOp) ]
            , Audio.cmdNone
            )

        GotStartTime time ->
            ( { model | playing = not model.playing, startTime = time }, Cmd.none, Audio.cmdNone )

        AudioLoaded _ ->
            ( model, Cmd.none, Audio.cmdNone )

        NoOp ->
            ( model, Cmd.none, Audio.cmdNone )


toggleWhiteNoise : LoadedModel -> LoadedModel
toggleWhiteNoise model =
    { model | playing = not model.playing }



---- VIEW ----


view : AudioData -> Model -> Html Msg
view audioData model =
    case model of
        Loading ->
            div [] [ text "Loading..." ]

        Errored error ->
            div [] [ text <| "Error: " ++ error ]

        Loaded loadedModel ->
            viewLoaded audioData loadedModel


viewLoaded : AudioData -> LoadedModel -> Html Msg
viewLoaded audioData model =
    div [ Attr.class "app" ]
        [ Html.button
            [ Ev.onClick ClickedToggle
            , Attr.id "toggle"
            , Attr.class "toggle"
            , Attr.class
                (if model.playing then
                    "toggle--on"

                 else
                    "toggle--off"
                )
            ]
            [ text "Toggle white noise" ]
        ]


audio : AudioData -> Model -> Audio
audio audioData model =
    case model of
        Loaded loadedModel ->
            if loadedModel.playing then
                let
                    audioLength =
                        Audio.length audioData loadedModel.noise
                in
                Audio.audioWithConfig (audioConfig audioLength) loadedModel.noise loadedModel.startTime

            else
                Audio.silence

        _ ->
            Audio.silence


audioConfig : Duration -> Audio.PlayAudioConfig
audioConfig audioLength =
    { loop = Just { loopStart = Quantity.zero, loopEnd = audioLength }
    , playbackRate = 1
    , startAt = Quantity.zero
    }


port audioPortToJS : Json.Encode.Value -> Cmd msg


port audioPortFromJS : (Json.Decode.Value -> msg) -> Sub msg


subscriptions : AudioData -> Model -> Sub Msg
subscriptions _ model =
    Browser.Events.onKeyUp
        (Json.Decode.field "key" Json.Decode.string
            |> Json.Decode.map
                (\key ->
                    if key == " " then
                        PressedSpace

                    else
                        NoOp
                )
        )



---- PROGRAM ----


main : Program () (Audio.Model Msg Model) (Audio.Msg Msg)
main =
    Audio.elementWithAudio
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , audio = audio
        , audioPort = { toJS = audioPortToJS, fromJS = audioPortFromJS }
        }
